# malmo-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Using docker

The included `Dockerfile` will build two versions of the app:

-  production
-  production-internal

It will also leave the node dev-dependencies on the built docker image - this 
enables using the same image for production and dev.

The docker image uses a custom `entrypoint.sh` script that uses the value of 
its first argument to decide how the app is served:

-  If the entrypoint is called with a `production` argument, then it will
   use nginx to serve the public version of the app on port 80 and the internal
   version of the app on port 8001 - both apps were previously built for 
   production during the `docker build`
   
-  If the entrypoint is called with any other value for its first argument, 
   then it will use `vue-cli-service serve` to serve the development version
   of the app on port 80

```
# running the docker image for dev in standalone mode
docker run  
    --rm \
    -ti \
    -p 8001:80 \
    -v $(pwd)/src:/app/src \
    -v$(pwd)/docker:/app/docker \
    -v $(pwd)/public:/app/public \
    malmo-ui-test dev
```


## Building the docker images

Since there are different values for the DNS of the `malmo-backend` depending whether we are in `dev` or `prod`, we 
build two different docker images, one for each environment. As such, build the `dev` image with:

```
docker build -t registry.gitlab.com/likeno/innometeo/malmo-ui:dev --build-arg ENVIRONMENT=development .
```
