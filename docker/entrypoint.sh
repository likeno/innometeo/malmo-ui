PROD="production"

ENVIRONMENT=${1:-$PROD}
PUBLIC=${2}

echo $ENVIRONMENT

if [ ${ENVIRONMENT} = $PROD ]
then
  exec nginx -c /app/docker/nginx.conf
else
  exec npx vue-cli-service serve --port 80
fi

