const path = require('path')
const webpack = require('webpack')

module.exports = {
  devServer: {
    allowedHosts: ["."],
    disableHostCheck: true,
  },
  chainWebpack: config => {
    const apiClient = process.env.NODE_ENV === 'production' ? 'server' : 'server'
    config.resolve.alias.set(
      'api-client',
      path.resolve(__dirname, `src/api/${apiClient}`)
    )

    const featureInfoClient = process.env.NODE_ENV === 'production' ? 'server' : 'mock'
    config.resolve.alias.set(
      'featureinfo-client',
      path.resolve(__dirname, `src/core/featureinfo/${apiClient}`)
    )
  }
}
