import Vue from 'vue'
import uuid from 'uuid'


const WORKSPACE_INITIAL_STATE = {
  layers: {},
  layerGroups: {},
  locations: {},
  locationGroups: {},
  ui: {
    currentPanel: null
  },
  time: {
    referenceTime: 0,
    step: 0,
  },
}

export const initializeWorkspace = (state, id) => {
  const workspaceState = { ...WORKSPACE_INITIAL_STATE, id: id }

  Object.values(state.api.layers).forEach(layer => {
    const layerProxyId = uuid()
    Vue.set(workspaceState.layers, layerProxyId, {
      ...layer,
      layerProxyId: layerProxyId,
    })
  })

  Object.values(state.api.layerGroups).forEach(layerGroup => {
    const layerGroupProxyId = uuid()
    Vue.set(workspaceState.layerGroups, layerGroupProxyId, {
      ...layerGroup,
      layerGroupProxyId: layerGroupProxyId
    })
  })

  Object.values(state.api.locations).forEach(location => {
    const locationProxyId = uuid()
    Vue.set(workspaceState.locations, locationProxyId, {
      ...location,
      locationProxyId: locationProxyId,
      active: false,
    })
  })

  Object.values(state.api.locationGroups).forEach(locationGroup => {
    const locationGroupProxyId = uuid()
    Vue.set(workspaceState.locationGroups, locationGroupProxyId, {
      ...locationGroup,
      locationGroupProxyId,
    })
  })

  return workspaceState
}
