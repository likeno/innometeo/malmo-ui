import Vue from 'vue'
import Vuex from 'vuex'
import client from 'api-client'

import { workspaceStoreMutations } from '@/workspace/store/mutations'
import { workspaceStoreGetters } from '@/workspace/store/getters'
import { workspaceStoreActions } from '@/workspace/store/actions'

Vue.use(Vuex)

// list of mutations
export const ADD_SERVICES = 'ADD_SERVICES'
export const ADD_LAYER_GROUPS = 'ADD_LAYER_GROUPS'
export const ADD_LAYERS = 'ADD_LAYERS'
export const ADD_PANES = 'ADD_PANES'
export const ADD_LOCATION_GROUPS = 'ADD_LOCATION_GROUPS'
export const ADD_LOCATIONS = 'ADD_LOCATIONS'

export const SET_LOADING_STATUS = 'SET_LOADING_STATUS'

export const LINK_WORKSPACES = 'LINK_WORKSPACES'


export default new Vuex.Store({
  state: {
    // Data retrieved from the backend API
    api: {
      services: {},
      layerGroups: {},
      layers: {},
      mapPanes: {},
      locationGroups: {},
      locations: {},
    },

    // Global UI data
    ui: {
      loading: true,
    },

    // Workspaces data
    workspaces: {},
    linked: false,

    // General global data
    defaultBasemap: 'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png',
  },

  getters: {
    ...workspaceStoreGetters,

    linked: (state) => {
      return state.linked
    },

    services: (state) => {
      return state.api.services
    },

    serviceById: (state) => (id) => {
      return state.api.services[id]
    },

    layerGroups: (state) => {
      return state.api.layerGroups
    },

    layerGroupById: (state) => (id) => {
      return state.api.layerGroups[id]
    },

    layers: (state) => {
      return state.api.layers
    },

    layerById: (state) => (id) => {
      return state.api.layers[id]
    },

    layersByGroupId: (state) => (group) => {
      return Object.values(state.api.layers).filter(layer => {
        return layer.groups.includes(group)
      })
    },

    mapPanes: (state) => {
      return state.api.mapPanes
    },

    mapPaneById: (state) => (id) => {
      return state.api.mapPanes[id]
    },


    loadingStatus: (state) => {
      return state.ui.loading
    },
  },

  mutations: {
    ...workspaceStoreMutations,

    [LINK_WORKSPACES](state, payload) {
      state.linked = payload.linked
    },

    [ADD_SERVICES](state, payload) {
      payload.services.forEach(service => {
        Vue.set(state.api.services, service.id, service)
      })
    },

    [ADD_LAYER_GROUPS](state, payload) {
      payload.layerGroups.forEach(layerGroup => {
        Vue.set(state.api.layerGroups, layerGroup.id, layerGroup)
      })
    },

    [ADD_LAYERS](state, payload) {
      payload.layers.forEach(layer => {
        Vue.set(state.api.layers, layer.id, layer)
      })
    },

    [ADD_PANES](state, payload) {
      payload.panes.forEach(pane => {
        Vue.set(state.api.mapPanes, pane.id, pane)
      })
    },

    [ADD_LOCATION_GROUPS](state, payload) {
      payload.locationGroups.forEach(locationGroup => {
        Vue.set(state.api.locationGroups, locationGroup.id, locationGroup)
      })
    },

    [ADD_LOCATIONS](state, payload) {
      payload.locations.forEach(location => {
        Vue.set(state.api.locations, location.id, location)
      })
    },

    [SET_LOADING_STATUS](state, payload) {
      state.ui.loading = payload.value
    },
  },

  actions: {
    ...workspaceStoreActions,

    fetchServices({ commit }) {
      return client.fetchServices()
        .then(services => commit(ADD_SERVICES, { services: services }))
    },

    fetchLayerGroups({ commit }) {
      return client.fetchLayerGroups()
        .then(layerGroups => commit(ADD_LAYER_GROUPS, { layerGroups: layerGroups }))
    },

    fetchLayers({ commit }) {
      return client.fetchLayers()
        .then(layers => commit(ADD_LAYERS, { layers: layers }))
    },

    fetchPanes({ commit }) {
      return client.fetchPanes()
        .then(panes => commit(ADD_PANES, { panes: panes }))
    },

    fetchLocationGroups({ commit }) {
      return client.fetchLocationGroups()
        .then(locationGroups => commit(ADD_LOCATION_GROUPS, { locationGroups: locationGroups }))
    },

    fetchLocations({ commit }) {
      return client.fetchLocations()
        .then(locations => commit(ADD_LOCATIONS, { locations: locations }))
    },
  },
})
