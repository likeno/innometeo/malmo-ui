export default {
  arome: {
    title: 'AROME',
    plots: [{
      title: 'Temperatura',
      variables: [
        'arome.temperature.continent.surface',
        'arome.temperature.continent.950',
        'arome.temperature.continent.925',
        'arome.temperature.continent.900',
      ]
    }, {
      title: 'Humidade relativa',
      variables: [
        'arome.relative_humidity.continent.surface',
        'arome.relative_humidity.continent.950',
        'arome.relative_humidity.continent.925',
        'arome.relative_humidity.continent.900',
      ]
    }, {
      title: 'Pressão',
      variables: [
        'arome.pressure.continent.surface',
      ]
    }, {
      title: 'Geopotencial',
      variables: [
        'arome.geopotential.continent.950',
        'arome.geopotential.continent.900',
        'arome.geopotential.continent.800',
      ]
    }]
  },

  ecmwf: {
    title: 'ECMWF',
    plots: [{
      title: 'Temperatura',
      variables: [
        'ecmwf.temperature.atlantic.surface',
        'ecmwf.temperature.atlantic.925',
        'ecmwf.temperature.atlantic.900',
        'ecmwf.temperature.atlantic.800',
      ]
    }, {
      title: 'Humidade',
      variables: [
        'ecmwf.relative_humidity.atlantic.surface',
        'ecmwf.relative_humidity.atlantic.950',
        'ecmwf.relative_humidity.atlantic.900',
        'ecmwf.relative_humidity.atlantic.800',
      ]
    }, {
      title: 'Pressão',
      variables: [
        'ecmwf.pressure.atlantic.surface',
      ]
    }, {
      title: 'Geopotencial',
      variables: [
        'ecmwf.geopotential.atlantic.950',
        'ecmwf.geopotential.atlantic.900',
        'ecmwf.geopotential.atlantic.800',
      ]
    }]
  }
}
