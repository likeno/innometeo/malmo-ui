export class QueryDataDatabase {
  constructor(latitude, longitude) {
    this.info = {
      latitude,
      longitude,
    }

    this.data = {}
  }
}
