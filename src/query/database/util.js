export const splitVariableNameIntoComponents = (name) => {
  let components = name.split('.')
  return { model: components[0], name: components[1], area: components[2] }
}
