export const getComponentsFromLayerName = (name) => {
  let components = name.split('.')
  return { model: components[0], name: components[1], level: components[2] }
}

export const processFeatureInfoData = (data, requestParams) => {
  let layers = Object.keys(data.properties)

  return layers.map(layerName => {
    let components = getComponentsFromLayerName(layerName)

    return data.properties[layerName].map(item => {
      return {
        ...components,
        ...item,
        reference_time: requestParams.reference_time,
      }
    })
  }).flat()
}
