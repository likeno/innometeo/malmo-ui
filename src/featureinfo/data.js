import { buildFeatureInfoRequestUrl, getFeatureInfo } from './request'
import { getComponentsFromLayerName, processFeatureInfoData } from './process'

export class FeatureInfoDatabase {
  constructor(locationId, latitude, longitude) {
    this.locationId = locationId
    this.latitude = latitude
    this.longitude = longitude
    this.data = []
    this.urlCache = []
  }

  async get(params) {
    let url = buildFeatureInfoRequestUrl({
      ...params,
      latitude: this.latitude,
      longitude: this.longitude,
    })

    if(!this._isUrlCached(url)) {
      try {
        let response = await getFeatureInfo(url)

        processFeatureInfoData(response.data, params).forEach(item => {
          this.data.push(item)
        })

        this.urlCache.push(url)
      } catch(error) {
        return { error: error, data: [] }
      }
    }


    return {
      error: null,
      data: this.data.filter(item => {
        let components = getComponentsFromLayerName(params.layer)
        return item.model === components.model &&
          item.name === components.name &&
          item.area === components.area &&
          item.level === params.level &&
          item.reference_time === params.reference_time &&
          params.time.includes(item.time)
      })
    }
  }

  _isUrlCached(url) {
    return this.urlCache.includes(url)
  }
}
