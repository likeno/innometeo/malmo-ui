// General utilities and methods for the GetFeatureInfo
import { FeatureInfoDatabase } from './data'


class FeatureInfoRegistry {
  constructor() {
    this.databases = {}
  }

  has(locationId) {
    return locationId in this.databases
  }

  add(locationId, latitude, longitude) {
    if(!this.has(locationId)) {
      this.databases[locationId] = new FeatureInfoDatabase(
        locationId, latitude, longitude
      )
    }

    return this.databases[locationId]
  }

  get(locationId) {
    return this.databases[locationId]
  }

  remove(locationId) {
    if(this.has(locationId)) {
      delete this.databases[locationId]
    }

    return this
  }
}

export const registry = new FeatureInfoRegistry()
