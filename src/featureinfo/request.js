import axios from 'axios'
import { buildQueryString, buildUrlWithQuery } from '@/core/url'

const QUERY_URL = process.env.VUE_APP_BASE_FEATUREINFO_URL

export const buildFeatureInfoRequestUrl = (params) => {
  return buildUrlWithQuery(QUERY_URL, buildQueryString(params))
}

export const getFeatureInfo = async (url) => {
  return await axios.get(url)
}
