import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Dashboard from '@/dashboard/Dashboard.vue'
import Detail from '@/detail/Detail.vue'
import DetailStation from '@/detail/DetailStation.vue'

export default new Router({
  routes: [{
    name: 'main',
    path: '/',
    component: Dashboard
  }, {
    name: 'detail',
    path: '/detail/:datetime/:latitude/:longitude',
    component: Detail
  }, {
    name: 'detailstation',
    path: '/detail/:datetime/:latitude/:longitude/:station',
    component: DetailStation
  }]
})
