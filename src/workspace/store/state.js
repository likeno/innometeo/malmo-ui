import uuid from 'uuid'
import { getCurrentTime, getReferenceTime } from '@/core/datetime'

export const LAYER_TYPE = {
  WMS: 0,
  STATION: 1
}


const WORKSPACE_INITIAL_STATE = () => {
  return {
    ui: {
      currentPanel: null,
    },

    panels: {
      layers: {
        currentGroup: null,
        minimized: false,
      },
      locations: {
        currentGroup: null,
      },
    },

    wms: {
      layers: [],
      panes: [],
      layerGroups: [],
      locations: [],
      locationGroups: [],
    },

    time: {
      referenceTime: getReferenceTime(),
      currentTime: getCurrentTime(),
    },

    local: null,
    query: null,

    pressureLevel: 'SURFACE',
    plotSettings: ['ecmwf.temperature.surface'],
  }
}


const WORKSPACE_QUERY_INITIAL_STATE = () => {
  return {
    requests: {},
    data: {},
    ui: {
      currentPanel: 'table-display-panel',
    },
  }
}


const buildWorkspaceWmsLayer = (layer) => {
  return {
    ...layer,
    uniqueId: uuid(),
  }
}


const buildWorkspacePane = (pane) => {
  return {
    ...pane,
    uniqueId: uuid(),
  }
}


const buildWorkspaceLayerGroup = (layerGroup) => {
  return {
    ...layerGroup,
    uniqueId: uuid(),
  }
}


const buildWorkspaceLocation = (location) => {
  return {
    ...location,
    uniqueId: uuid(),
    status: false,
  }
}


const buildWorkspaceLocationGroup = (locationGroup) => {
  return {
    ...locationGroup,
    uniqueId: uuid(),
  }
}


export const initializeWorkspaceQueryObject = (queryInfo) => {
  return {
    ...WORKSPACE_QUERY_INITIAL_STATE(),
    query: queryInfo
  }
}


export const initializeWorkspaceState = (state, workspaceId) => {
  let workspaceState = { ...WORKSPACE_INITIAL_STATE(), id: workspaceId }

  Object.values(state.api.layers).forEach(apiLayer => {
    workspaceState.wms.layers.push(buildWorkspaceWmsLayer(apiLayer))
  })

  Object.values(state.api.mapPanes).forEach(apiPane => {
    workspaceState.wms.panes.push(buildWorkspacePane(apiPane))
  })

  Object.values(state.api.layerGroups).forEach(apiLayerGroup => {
    workspaceState.wms.layerGroups.push(buildWorkspaceLayerGroup(apiLayerGroup))
  })

  Object.values(state.api.locations).forEach(apiLocation => {
    workspaceState.wms.locations.push(buildWorkspaceLocation(apiLocation))
  })

  Object.values(state.api.locationGroups).forEach(apiLocationGroup => {
    workspaceState.wms.locationGroups.push(buildWorkspaceLocationGroup(apiLocationGroup))
  })

  workspaceState.panels.layers.currentGroup = workspaceState.wms.layerGroups[0].uniqueId
  workspaceState.panels.locations.currentGroup = workspaceState.wms.locationGroups[0].uniqueId

  return workspaceState
}
