import Vue from 'vue'
import uuid from 'uuid'
import { initializeWorkspaceState, initializeWorkspaceQueryObject } from './state'
import { EventBus } from '@/bus'

export const CREATE_WORKSPACE = 'CREATE_WORKSPACE'
export const DELETE_WORKSPACE = 'DELETE_WORKSPACE'
export const SET_WORKSPACE_CURRENT_PANEL = 'SET_WORKSPACE_CURRENT_PANEL'
export const SET_WORKSPACE_LAYER_STATUS = 'SET_WORKSPACE_LAYER_STATUS'
export const SET_WORKSPACE_ACTIVE_LAYER_GROUP = 'SET_WORKSPACE_ACTIVE_LAYER_GROUP'
export const SET_WORKSPACE_LOCATION_STATUS = 'SET_WORKSPACE_LOCATION_STATUS'
export const SET_WORKSPACE_ACTIVE_LOCATION_GROUP = 'SET_WORKSPACE_ACTIVE_LOCATION_GROUP'
export const SET_WORKSPACE_PRESSURE_LEVEL = 'SET_WORKSPACE_PRESSURE_LEVEL'
export const SET_WORKSPACE_LAYERS_PANEL_MINIMIZED = 'SET_WORKSPACE_LAYERS_PANEL_MINIMIZED'
export const SET_WORKSPACE_TIME_STEP = 'SET_WORKSPACE_TIME_STEP'
export const SET_WORKSPACE_REFERENCE_TIME = 'SET_WORKSPACE_REFERENCE_TIME'
export const SET_WORKSPACE_CURRENT_TIME = 'SET_WORKSPACE_CURRENT_TIME'
export const SET_WORKSPACE_QUERY = 'SET_WORKSPACE_QUERY'
export const SET_WORKSPACE_QUERY_CURRENT_PANEL = 'SET_WORKSPACE_QUERY_CURRENT_PANEL'
export const SET_WORKSPACE_QUERY_REQUEST_CACHE = 'SET_WORKSPACE_QUERY_REQUEST_CACHE'
export const SET_WORKSPACE_CURRENT_PLOT_SETTINGS = 'SET_WORKSPACE_CURRENT_PLOT_SETTINGS'
export const ADD_WORKSPACE_CUSTOM_LOCAL = 'ADD_WORKSPACE_CUSTOM_LOCAL'
export const REMOVE_WORKSPACE_CUSTOM_LOCAL = 'REMOVE_WORKSPACE_CUSTOM_LOCAL'


export const workspaceStoreMutations = {
  [CREATE_WORKSPACE](state) {
    const workspaceId = uuid()
    Vue.set(state.workspaces, workspaceId, initializeWorkspaceState(state, workspaceId))
  },

  [DELETE_WORKSPACE](state, payload) {
    Vue.delete(state.workspaces, payload.workspaceId)
  },

  [SET_WORKSPACE_CURRENT_PANEL](state, payload) {
    state.workspaces[payload.workspaceId].ui.currentPanel = payload.panel
    EventBus.$emit('reload-map-size', { workspaceId: payload.workspaceId })
  },

  [SET_WORKSPACE_LAYER_STATUS](state, payload) {
    state.workspaces[payload.workspaceId].wms.layers.find(item => {
      return item.uniqueId === payload.layerId
    }).status = payload.value
  },

  [SET_WORKSPACE_ACTIVE_LAYER_GROUP](state, payload) {
    state.workspaces[payload.workspaceId].panels.layers.currentGroup = payload.layerGroupId
  },

  [SET_WORKSPACE_LOCATION_STATUS](state, payload) {
    state.workspaces[payload.workspaceId].wms.locations.find(item => {
      return item.uniqueId === payload.locationId
    }).status = payload.value
  },

  [SET_WORKSPACE_ACTIVE_LOCATION_GROUP](state, payload) {
    state.workspaces[payload.workspaceId].panels.locations.currentGroup = payload.locationGroupId
  },

  [SET_WORKSPACE_PRESSURE_LEVEL](state, payload) {
    state.workspaces[payload.workspaceId].pressureLevel = payload.pressureLevel
  },

  [SET_WORKSPACE_LAYERS_PANEL_MINIMIZED](state, payload) {
    state.workspaces[payload.workspaceId].panels.layers.minimized = payload.value
  },

  [SET_WORKSPACE_TIME_STEP](state, payload) {
    if(payload.value > -1 && payload.value < 72)
      state.workspaces[payload.workspaceId].time.step = payload.value
  },

  [SET_WORKSPACE_REFERENCE_TIME](state, payload) {
    state.workspaces[payload.workspaceId].time.referenceTime = payload.referenceTime
  },

  [SET_WORKSPACE_CURRENT_TIME](state, payload) {
    state.workspaces[payload.workspaceId].time.currentTime = payload.currentTime
  },

  [SET_WORKSPACE_QUERY](state, payload) {
    window.console.log(payload)
    if(state.workspaces[payload.workspaceId].query === null) {
      window.console.log('init query')
      state.workspaces[payload.workspaceId].query = initializeWorkspaceQueryObject(payload.query)
    } else if(state.workspaces[payload.workspaceId].query.query.latitude === payload.query.latitude && state.workspaces[payload.workspaceId].query.query.longitude === payload.query.longitude) {
      window.console.log('close query')
      state.workspaces[payload.workspaceId].query = null
    } else {
      window.console.log('new query')
      state.workspaces[payload.workspaceId].query = initializeWorkspaceQueryObject(payload.query)
    }
  },

  [SET_WORKSPACE_QUERY_CURRENT_PANEL](state, payload) {
    state.workspaces[payload.workspaceId].query.ui.currentPanel = payload.panel
  },

  [SET_WORKSPACE_QUERY_REQUEST_CACHE](state, payload) {
    state.workspaces[payload.workspaceId].query.requests[payload.requestHash] = payload.response

    Object.keys(payload.response.data.properties).forEach((key) => {
      state.workspaces[payload.workspaceId].query.data[key] = {}

      let data = payload.response.data.properties[key]
      data.forEach((item) => {
        if(item.time in state.workspaces[payload.workspaceId].query.data[key]) {
          state.workspaces[payload.workspaceId].query.data[key][item.time][item.level] = item.value
        } else {
          state.workspaces[payload.workspaceId].query.data[key][item.time] = {}
          state.workspaces[payload.workspaceId].query.data[key][item.time][item.level] = item.value
        }
      })
    })
  },

  [SET_WORKSPACE_CURRENT_PLOT_SETTINGS](state, payload) {
    state.workspaces[payload.workspaceId].plotSettings = payload.plotSettings
  },

  [ADD_WORKSPACE_CUSTOM_LOCAL](state, payload) {
    state.workspaces[payload.workspaceId].wms.locations.push({
      ...payload.location,
      groups: [],
      status: false,
    })

    state.workspaces[payload.workspaceId].local = payload.location.uniqueId
  },

  [REMOVE_WORKSPACE_CUSTOM_LOCAL](state, payload) {
    Vue.set(state.workspaces[payload.workspaceId].wms,
      'locations',
      state.workspaces[payload.workspaceId].wms.locations.filter((n) => {
        window.console.log(n, state.workspaces[payload.workspaceId].local)
        return n.uniqueId !== state.workspaces[payload.workspaceId].local
      })
    )
    state.workspaces[payload.workspaceId].local = null
  }
}
