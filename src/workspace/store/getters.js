export const workspaceStoreGetters = {
  workspaces: (state) => {
    return state.workspaces
  },

  workspaceById: (state) => (workspaceId) => {
    return state.workspaces[workspaceId]
  },

  workspaceCount: (state) => {
    return Object.keys(state.workspaces).length
  },

  workspaceLayers: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.layers
  },

  workspaceLayerById: (state) => (workspaceId, layerId) => {
    return state.workspaces[workspaceId].wms.layers.find(item => {
      return item.uniqueId === layerId
    })
  },

  workspacePanes: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.panes
  },

  workspacePaneById: (state) => (workspaceId, paneId) => {
    return state.workspaces[workspaceId].wms.panes.find(item => {
      return item.uniqueId === paneId
    })
  },

  workspaceLayerGroups: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.layerGroups
  },

  workspaceLayerGroupById: (state) => (workspaceId, layerGroupId) => {
    return state.workspaces[workspaceId].wms.layerGroups.find(item => {
      return item.uniqueId === layerGroupId
    })
  },

  workspaceLocations: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.locations
  },

  workspaceLocationById: (state) => (workspaceId, locationId) => {
    return state.workspaces[workspaceId].wms.locations.find(item => {
      return item.uniqueId === locationId
    })
  },

  workspaceLocationGroups: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.locationGroups
  },

  workspaceLocationGroupById: (state) => (workspaceId, locationGroupId) => {
    return state.workspaces[workspaceId].wms.locationGroups.find(item => {
      return item.uniqueId === locationGroupId
    })
  },

  workspaceCurrentPanel: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].ui.currentPanel
  },

  workspaceLayersPanelMinimized: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].panels.layers.minimized
  },

  workspaceActiveLayerGroup: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].panels.layers.currentGroup
  },

  workspaceActiveLocationGroup: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].panels.locations.currentGroup
  },

  workspaceReferenceTime: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].time.referenceTime
  },

  workspaceCurrentTime: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].time.currentTime
  },

  workspaceStep: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].time.step
  },

  workspacePressureLevel: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].pressureLevel
  },

  workspaceQuery: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].query
  },

  workspaceQueryRequestCache: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].query.requests
  },

  workspaceQueryCurrentPanel: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].query.ui.currentPanel
  },

  workspaceCurrentPlotSettings: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].plotSettings
  },

  workspaceCustomLocation: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].local
  },
}
