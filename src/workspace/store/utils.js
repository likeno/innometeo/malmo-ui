import uuid from 'uuid'

export function createStationsLayerGroup () {
  return {
    id: uuid(),
    title: 'Estações',
    uniqueId: uuid()
  }
}


export function createStationsLayers () {
  return [{
    id: uuid(),
    uniqueId: uuid(),
    url: process.env.VUE_APP_BASE_OBSERVATIONS_URL,
    name: 'temperature-mean',
    title: 'Temperatura média',
    type: 2
  }]
}
