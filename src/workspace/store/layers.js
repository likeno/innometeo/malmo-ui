export const getters = {
  wmsLayers: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.layers.filter(l => l.type === LAYER_TYPE.WMS)
  },

  stationLayers: (state) => (workspaceId) => {
    return state.workspaces[workspaceId].wms.layers.filter(l => l.type === LAYER_TYPE.STATION)
  },

  layerByType: (state) => (workspaceId, type) => {
    return state.workspaces[workspaceId].wms.layers.filter(l => l.type === type)
  },
}
