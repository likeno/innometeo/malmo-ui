import { SET_WORKSPACE_QUERY_REQUEST_CACHE } from '@/workspace/store/mutations'

export const workspaceStoreActions = {
  setWorkspaceQueryRequestCache({ commit }, payload) {
    commit(SET_WORKSPACE_QUERY_REQUEST_CACHE, { ...payload })
  },
}
