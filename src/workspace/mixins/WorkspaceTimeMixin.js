import moment from 'moment'

export default {
  data () {
    return {
      referenceTime: this.getInitialReferenceTime().unix(),
      time: this.getInitialTime().unix()
    }
  },

  computed: {
    referenceDate: {
      get () {
        return moment.unix(this.referenceTime).startOf('day').unix()
      }
    },

    referenceHour: {
      get () {
        return moment.unix(this.referenceTime).hour()
      }
    }
  },

  methods: {
    getInitialReferenceTime () {
      const now = moment.utc()

      if (now.hour() < 11) {
        return now.startOf('day').subtract(12, 'hours')
      } else if (now.hour() >= 21) {
        return now.startOf('day').add(12, 'hours')
      }

      return now.startOf('day')
    },

    getInitialTime () {
      const now = moment.utc()
      return now.startOf('hour')
    }
  }
}
