export default {
  computed: {
    leafletMap: {
      get() {
        return this.__leafletMap
      }
    }
  }
}
