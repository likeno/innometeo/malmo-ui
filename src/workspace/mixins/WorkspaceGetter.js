export default {
  computed: {
    workspaceId: {
      get () {
        return this.$parent.workspaceId
      }
    },

    workspaceStore: {
      get () {
        return this.$parent.workspaceStore
      }
    },

    workspaceComponent: {
      get () {
        return this.$parent.workspaceComponent
      }
    }
  },
}
