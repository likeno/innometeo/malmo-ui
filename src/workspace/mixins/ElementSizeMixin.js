export default {
  methods: {
    getElementDimensions () {
      return this.$el.getBoundingClientRect()
    },

    getElementWidth () {
      return this.getElementDimensions().width
    },

    getElementHeight () {
      return this.getElementDimensions().height
    }
  }
}
