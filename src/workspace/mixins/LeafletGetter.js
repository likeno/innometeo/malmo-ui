export default {
  computed: {
    leafletMap: {
      get() {
        return this.$parent.leafletMap
      }
    }
  }
}
