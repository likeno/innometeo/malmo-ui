export default {
  props: {
    workspaceId: {
      type: String,
      required: true,
    }
  },

  computed: {
    workspaceStore: {
      get () {
        return this.$store.getters.workspaceById(this.workspaceId)
      }
    },

    workspaceComponent: {
      get () {
        return this
      }
    }
  },
}
