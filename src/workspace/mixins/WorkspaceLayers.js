import uuid from 'uuid'
import { LayerType } from '@/core/layers'

export default {
  data () {
    return {
      layers: this.loadLayers(),
      layerGroups: []
    }
  },

  computed: {
    wmsLayers: {
      get () {
        return this.layers.filter(layer => layer.type === LayerType.WMS)
      }
    },

    stationLayers: {
      get () {
        return this.layers.filter(layer => layer.type === LayerType.STATION)
      }
    }
  },

  methods: {
    loadLayers () {
      return [
        ...this.loadAPILayers(),
        ...this.loadStationLayers()
      ]
    },

    loadAPILayers () {
      return Object.values(this.$store.getters.layers).map(layer => this.loadAPILayer(layer))
    },

    loadAPILayer (layer) {
      return {
        ...layer,
        type: LayerType.WMS,
        uniqueId: uuid()
      }
    },

    loadStationLayers () {
      return []
    },

    loadStationLayer (layer) {
      return {
        ...layer,
        type: LayerType.STATION,
        uniqueId: uuid()
      }
    }
  }
}
