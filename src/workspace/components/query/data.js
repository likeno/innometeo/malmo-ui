const temperatureData = [{
  datetime: '2019-11-26T00:00:00',
  value: 4.56
}, {
  datetime: '2019-11-26T01:00:00',
  value: 3.2
}, {
  datetime: '2019-11-26T02:00:00',
  value: 1.1
}, {
  datetime: '2019-11-26T03:00:00',
  value: 2.0
}, {
  datetime: '2019-11-26T04:00:00',
  value: 4.92
}, {
  datetime: '2019-11-26T05:00:00',
  value: 3.23
}, {
  datetime: '2019-11-26T06:00:00',
  value: 1.23
}]


const temperatureAxis = {
  id: 'temperatureAxis',
  scaleLabel: {
    display: true,
    labelString: 'Temperatura [ºC]',
  },
  ticks: {
    suggestedMax: 20,
  }
}


const pressureData = [{
  datetime: '2019-11-26T00:00:00',
  value: 1012.23
}, {
  datetime: '2019-11-26T01:00:00',
  value: 1001.90
}, {
  datetime: '2019-11-26T02:00:00',
  value: 1008.12
}, {
  datetime: '2019-11-26T03:00:00',
  value: 1009.01
}, {
  datetime: '2019-11-26T04:00:00',
  value: 1011.15
}, {
  datetime: '2019-11-26T05:00:00',
  value: 1013.75
}, {
  datetime: '2019-11-26T06:00:00',
  value: 1012.98
}]


const pressureAxis = {
  id: 'pressureAxis',
  scaleLabel: {
    display: true,
    labelString: 'Pressão [hPa]',
  },
  ticks: {
    suggestedMin: 900,
    suggestedMax: 1020,
  }
}


const humidityData = [{
  datetime: '2019-11-26T00:00:00',
  value: 67.9
}, {
  datetime: '2019-11-26T01:00:00',
  value: 71.4
}, {
  datetime: '2019-11-26T02:00:00',
  value: 70.12
}, {
  datetime: '2019-11-26T03:00:00',
  value: 53.85
}, {
  datetime: '2019-11-26T04:00:00',
  value: 51.20
}, {
  datetime: '2019-11-26T05:00:00',
  value: 69.1
}, {
  datetime: '2019-11-26T06:00:00',
  value: 79.4
}]


const humidityAxis = {
  id: 'humidityAxis',
  scaleLabel: {
    display: true,
    labelString: 'Humidade [%]',
  },
  ticks: {
    beginAtZero: true,
  }
}


const dataMapping = {
  temperature: temperatureData,
  pressure: pressureData,
  humidity: humidityData,
}


const axisMapping = {
  'arome.temperature.continent.surface': temperatureAxis,
  'arome.temperature.continent.950': temperatureAxis,
  'arome.temperature.continent.925': temperatureAxis,
  'arome.temperature.continent.900': temperatureAxis,
  'arome.temperature.continent.850': temperatureAxis,
  'arome.temperature.continent.800': temperatureAxis,
  'arome.temperature.continent.700': temperatureAxis,
  'arome.temperature.continent.500': temperatureAxis,
  'arome.temperature.continent.300': temperatureAxis,
  temperature: temperatureAxis,
  pressure: pressureAxis,
  humidity: humidityAxis,
}


const labelMapping = {
  temperature: 'Temperatura',
  pressure: 'Pressão',
  humidity: 'Humidade',
}


const colorMapping = {
  temperature: {
    background: 'rgba(0, 0, 0, 0)',
    border: '#d82424'
  },
  pressure: {
    background: 'rgba(0.1, 0.1, 0.45, 0.45)',
    border: 'rgba(0.1, 0.1, 0.45, 0.45)',
  },
  humidity: {
    background: 'rgba(73, 141, 209, 0.85)',
    border: 'rgba(73, 141, 209, 0.85)',
  },
}


export function dataProvider(name) {
  return dataMapping[name]
}

export function axisProvider(name) {
  return axisMapping[name]
}

export function labelProvider(name) {
  return labelMapping[name]
}

export function colorProvider(name) {
  return colorMapping[name]
}


export const axisOptions = {
  'temperatureAxis': {
    id: 'temperatureAxis',
    scaleLabel: {
      display: true,
      labelString: 'Temperatura [ºC]',
    },
    ticks: {
      suggestedMax: 20,
    }
  },
}

export const datasetOptions = {
  'arome.temperature.continent.surface': {
    label: 'Temperatura à superficie',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.950': {
    label: 'Temperatura a 950 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.925': {
    label: 'Temperatura a 925 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.900': {
    label: 'Temperatura a 900 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.850': {
    label: 'Temperatura a 850 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.800': {
    label: 'Temperatura a 800 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.700': {
    label: 'Temperatura a 700 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.500': {
    label: 'Temperatura a 500 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
  'arome.temperature.continent.300': {
    label: 'Temperatura a 300 hPa',
    yAxisID: 'temperatureAxis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: '#d82424',
  },
}
