import { findParentComponentByName } from '@/core/util'

// Simple mixin that augments a component with the ability to retrieve its
// parent workspace component
export default {
  methods: {
    getParentWorkspace() {
      return findParentComponentByName(this.$parent, 'workspacearea')
    }
  },

  computed: {
    // Computed property with direct access to the parent workspace identifier.
    workspace: {
      get() {
        return this.getParentWorkspace().identifier
      }
    }
  },
}
