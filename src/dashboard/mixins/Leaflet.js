import { map, tileLayer } from 'leaflet'

export default {
  mounted() {
    this.mapObject = map(this.$el, {
      zoom: 6,
      center: [37.5, -16]
    })
  },

  methods: {
    createBasemap(url, options) {
      tileLayer(url, options).addTo(this.mapObject)
    }
  },
}
