import moment from 'moment'

export const DEFAULT_CHART_OPTIONS = {
  responsive: true,
  maintainAspectRatio: false,
  tooltips: {
    mode: 'index',
    intersect: false,
    callbacks: {
      title: (item) => {
        return moment(item[0].label).format('YYYY-MM-DD HH:mm')
      },
      label: (tooltipItem, data) => {
        let label = data.datasets[tooltipItem.datasetIndex].label || ''

        if(label) {
          label += ': '
        }

        label += Math.round(tooltipItem.yLabel * 100) / 100
        return label
      }
    }
  },
  hover: {
    mode: 'nearest',
    intersect: true,
  },
  scales: {
    xAxes: [{
      type: 'time',
      time: {
        unit: 'hour',
        displayFormats: {
          hour: 'H[h]'
        }
      },
      scaleLabel: {
        display: true,
        labelString: 'Data e hora'
      },
      gridLines: {
        display: false
      }
    }],
    yAxes: [],
  }
}
