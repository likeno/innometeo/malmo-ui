import axios from 'axios'
import { buildQueryString, buildUrlWithQuery } from '@/core/url'

const QUERY_URL = process.env.VUE_APP_BASE_FEATUREINFO_URL

export const buildQueryUrl = (params) => {
  return buildUrlWithQuery(QUERY_URL, buildQueryString(params))
}

export const getFeatureinfo = async (url) => {
  return await axios.get(url)
}
