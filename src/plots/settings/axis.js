const temperatureAxis = {
  id: 'temperature-axis',
  scaleLabel: {
    display: true,
    labelString: 'Temperatura [ºC]'
  },
  ticks: {
    suggestedMax: 25
  },
  gridLines: {
    display: false
  }
}


const pressureAxis = {
  id: 'pressure-axis',
  scaleLabel: {
    display: true,
    labelString: 'Pressão [hPa]'
  },
  ticks: {
    suggestedMin: 1000,
    suggestedMax: 1020
  },
  gridLines: {
    display: false
  }
}


const humidityAxis = {
  id: 'humidity-axis',
  scaleLabel: {
    display: true,
    labelString: 'Humidade [%]'
  },
  ticks: {
    beginAtZero: true
  },
  gridLines: {
    display: false
  }
}


const verticalVelocityAxis = {
  id: 'vertical-velocity-axis',
  scaleLabel: {
    display: true,
    labelString: 'Velocidade vertical [hPa/hr]'
  },
  ticks: {
    suggestedMin: 0
  },
  gridLines: {
    display: false
  }
}


const geopotentialAxis = {
  id: 'geopotential-axis',
  scaleLabel: {
    display: true,
    labelString: 'Geopotencial'
  },
  ticks: {
    beginAtZero: true
  },
  gridLines: {
    display: false
  }
}


const cloudCoverAxis = {
  id: 'cloud-cover-axis',
  scaleLabel: {
    display: true,
    labelString: 'Cloud cover'
  },
  ticks: {
    beginAtZero: true
  },
  gridLines: {
    display: false
  }
}


const precipitationAxis = {
  id: 'precipitation-axis',
  scaleLabel: {
    display: true,
    labelString: 'Precipitação [mm]'
  },
  ticks: {
    beginAtZero: true
  },
  gridLines: {
    display: false
  }
}


export default {
  temperatureAxis,
  pressureAxis,
  humidityAxis,
  verticalVelocityAxis,
  geopotentialAxis,
  cloudCoverAxis,
  precipitationAxis,
  buiAxis: {
    id: 'bui-axis',
    scaleLabel: {
      display: true,
      labelString: 'Build-up Index'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  dcAxis: {
    id: 'dc-axis',
    scaleLabel: {
      display: true,
      labelString: 'Drought Code'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  dmcAxis: {
    id: 'dmc-axis',
    scaleLabel: {
      display: true,
      labelString: 'Duff Moisture Code'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  ffmcAxis: {
    id: 'ffmc-axis',
    scaleLabel: {
      display: true,
      labelString: 'Fine Fuel Moisture Content'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  fwiAxis: {
    id: 'fwi-axis',
    scaleLabel: {
      display: true,
      labelString: 'Fire Weather Index'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  isiAxis: {
    id: 'isi-axis',
    scaleLabel: {
      display: true,
      labelString: 'Initial Spread Index'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  p2000Axis: {
    id: 'p2000-axis',
    scaleLabel: {
      display: true,
      labelString: 'Extreme Probabilities'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  p2000aAxis: {
    id: 'p2000a-axis',
    scaleLabel: {
      display: true,
      labelString: 'Extreme Probabilities Anomalies'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },
  fireRiskAxis: {
    id: 'frm-axis',
    scaleLabel: {
      display: true,
      labelString: 'Fire Risk Map'
    },
    ticks: {
      beginAtZero: true,
    },
    gridLines: {
      display: false
    }
  },

}
