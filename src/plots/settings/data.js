// Temperature settings

const temperatureSurface = {
  order: 2,
  type: 'line',
  label: 'Temperatura à superficie',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#fa0000',
  yAxisID: 'temperature-axis'
}

const temperature950 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 950 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#e60a15',
  yAxisID: 'temperature-axis'
}

const temperature925 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 925 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#d20a28',
  yAxisID: 'temperature-axis'
}

const temperature900 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 900 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#be143c',
  yAxisID: 'temperature-axis'
}

const temperature850 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 850 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#aa1450',
  yAxisID: 'temperature-axis'
}

const temperature800 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 800 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#961464',
  yAxisID: 'temperature-axis'
}

const temperature700 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 700 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#821e78',
  yAxisID: 'temperature-axis'
}

const temperature500 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 500 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#6f1e8c',
  yAxisID: 'temperature-axis'
}

const temperature300 = {
  order: 2,
  type: 'line',
  label: 'Temperatura a 300 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#5a288c',
  yAxisID: 'temperature-axis'
}

// Geopotential settings

const geopotential950 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 950 hPa',
  backgroundColor: 'rgba(128, 0, 255, 250)',
  borderColor: 'rgba(128, 0, 255, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential925 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 925 hPa',
  backgroundColor: 'rgba(134, 43, 224, 250)',
  borderColor: 'rgba(134, 43, 224, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential900 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 900 hPa',
  backgroundColor: 'rgba(144, 90, 196, 250)',
  borderColor: 'rgba(144, 90, 196, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential850 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 850 hPa',
  backgroundColor: 'rgba(147, 118, 176, 250)',
  borderColor: 'rgba(147, 118, 176, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential800 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 800 hPa',
  backgroundColor: 'rgba(176, 118, 175, 250)',
  borderColor: 'rgba(176, 118, 175, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential700 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 700 hPa',
  backgroundColor: 'rgba(181, 87, 179, 250)',
  borderColor: 'rgba(181, 87, 179, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential500 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 500 hPa',
  backgroundColor: 'rgba(191, 57, 188, 250)',
  borderColor: 'rgba(191, 57, 188, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

const geopotential300 = {
  order: 2,
  type: 'line',
  fill: 'start',
  label: 'Geopotencial a 300 hPa',
  backgroundColor: 'rgba(163, 23, 160, 250)',
  borderColor: 'rgba(163, 23, 160, 250)',
  pointRadius: 0,
  yAxisID: 'geopotential-axis'
}

// Humidity settings

const humiditySurface = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa à superficie',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#0000fa',
  yAxisID: 'humidity-axis'
}

const humidity950 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 950 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#1300e6',
  yAxisID: 'humidity-axis'
}

const humidity925 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 925 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#280ad2',
  yAxisID: 'humidity-axis'
}

const humidity900 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 900 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#3d0abe',
  yAxisID: 'humidity-axis'
}

const humidity850 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 850 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#5014aa',
  yAxisID: 'humidity-axis'
}

const humidity800 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 800 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#3c28be',
  yAxisID: 'humidity-axis'
}

const humidity700 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 700 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#503cbe',
  yAxisID: 'humidity-axis'
}

const humidity500 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 500 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#6446aa',
  yAxisID: 'humidity-axis'
}

const humidity300 = {
  order: 2,
  type: 'line',
  label: 'Humidade relativa a 300 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#784696',
  yAxisID: 'humidity-axis'
}

// Vertical velocity

const verticalVelocity950 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 950 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#179639',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity925 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 925 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#26823e',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity900 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 900 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#2f6b3f',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity850 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 850 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#335c3e',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity800 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 800 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#435c33',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity700 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 700 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#4c7332',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity500 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 500 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#4f872a',
  yAxisID: 'vertical-velocity-axis'
}

const verticalVelocity300 = {
  order: 2,
  type: 'line',
  label: 'Velocidade vertical a 300 hPa',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#4c961b',
  yAxisID: 'vertical-velocity-axis'
}

// Pressure

const pressure = {
  order: 3,
  type: 'line',
  fill: 'start',
  label: 'Pressão ao N.M.M.',
  backgroundColor: 'rgba(30, 90, 130, 0.45)',
  borderColor: 'rgba(30, 90, 130, 0.45)',
  pointRadius: 0,
  yAxisID: 'pressure-axis',
}

// Dewpoint

const dewpoint = {
  order: 2,
  type: 'line',
  label: 'Temperatura do ponto de orvalho',
  backgroundColor: 'rgba(0, 0, 0, 0)',
  borderColor: '#236125',
  yAxisID: 'temperature-axis',
}

// Precipitation

const precipitation = {
  order: 2,
  type: 'bar',
  label: 'Precipitação',
  backgroundColor: 'rgba(112, 172, 196, 200)',
  borderColor: 'rgba(112, 172, 196, 200)',
  yAxisID: 'precipitation-axis',
}


export default {
  temperatureSurface,
  temperature950,
  temperature925,
  temperature900,
  temperature850,
  temperature800,
  temperature700,
  temperature500,
  temperature300,
  humiditySurface,
  humidity950,
  humidity925,
  humidity900,
  humidity850,
  humidity800,
  humidity700,
  humidity500,
  humidity300,
  geopotential950,
  geopotential925,
  geopotential900,
  geopotential850,
  geopotential800,
  geopotential700,
  geopotential500,
  geopotential300,
  verticalVelocity950,
  verticalVelocity925,
  verticalVelocity900,
  verticalVelocity850,
  verticalVelocity800,
  verticalVelocity700,
  verticalVelocity500,
  verticalVelocity300,
  pressure,
  dewpoint,
  precipitation,
  bui: {
    order: 2,
    type: 'line',
    label: 'BUI',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(66, 91, 172, 200)',
    yAxisID: 'bui-axis',
    steppedLine: 'before',
  },
  dc: {
    order: 2,
    type: 'line',
    label: 'DC',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(181,196,30, 200)',
    yAxisID: 'dc-axis',
  },
  dmc: {
    order: 2,
    type: 'line',
    label: 'DMC',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(196,130,130, 200)',
    yAxisID: 'dmc-axis',
  },
  ffmc: {
    order: 2,
    type: 'line',
    label: 'FFMC',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(101,100,196,200)',
    yAxisID: 'ffmc-axis',
  },
  fwi: {
    order: 2,
    type: 'line',
    label: 'FWI',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(50,93,196,200)',
    yAxisID: 'fwi-axis',
  },
  isi: {
    order: 2,
    type: 'line',
    label: 'ISI',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(196,64,75,200)',
    yAxisID: 'isi-axis',
  },
  p2000: {
    order: 2,
    type: 'line',
    label: 'P2000',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(125,196,178,200)',
    yAxisID: 'p2000-axis',
  },
  p2000a: {
    order: 2,
    type: 'line',
    label: 'P2000a',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(42,196,74,200)',
    yAxisID: 'p2000a-axis',
  },
  frm: {
    order: 2,
    type: 'line',
    label: 'FRM',
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: 'rgba(188,196,35,200)',
    yAxisID: 'frm-axis',
  },
}
