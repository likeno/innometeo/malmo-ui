import data from './data'
import process from './process'

export default {
  
  temperature: {
      
    surface: {
      dataType: data.temperatureSurface,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    950: {
      dataType: data.temperature950,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    925: {
      dataType: data.temperature925,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    900: {
      dataType: data.temperature900,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    850: {
      dataType: data.temperature850,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    800: {
      dataType: data.temperature800,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    700: {
      dataType: data.temperature700,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    500: {
      dataType: data.temperature500,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    },
    
    300: {
      dataType: data.temperature300,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    }

  },

  relative_humidity: {

    surface: {
      dataType: data.humiditySurface,
      process: (values) => {
        return values
      },
    },
    
    950: {
      dataType: data.humidity950,
      process: (values) => {
        return values
      },
    },
    
    925: {
      dataType: data.humidity925,
      process: (values) => {
        return values
      },
    },
    
    900: {
      dataType: data.humidity900,
      process: (values) => {
        return values
      },
    },
    
    850: {
      dataType: data.humidity850,
      process: (values) => {
        return values
      },
    },
    
    800: {
      dataType: data.humidity800,
      process: (values) => {
        return values
      },
    },
    
    700: {
      dataType: data.humidity700,
      process: (values) => {
        return values
      },
    },
    
    500: {
      dataType: data.humidity500,
      process: (values) => {
        return values
      },
    },
    
    300: {
      dataType: data.humidity300,
      process: (values) => {
        return values
      },
    }

  },


  vertical_velocity: {

    950: {
      dataType: data.verticalVelocity950,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    925: {
      dataType: data.verticalVelocity925,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    900: {
      dataType: data.verticalVelocity900,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    850: {
      dataType: data.verticalVelocity850,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    800: {
      dataType: data.verticalVelocity800,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    700: {
      dataType: data.verticalVelocity700,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    500: {
      dataType: data.verticalVelocity500,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    300: {
      dataType: data.verticalVelocity300,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    }

  },


  geopotential: {
      
    950: {
      dataType: data.geopotential950,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    925: {
      dataType: data.geopotential925,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    900: {
      dataType: data.geopotential900,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    850: {
      dataType: data.geopotential850,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    800: {
      dataType: data.geopotential800,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    700: {
      dataType: data.geopotential700,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    500: {
      dataType: data.geopotential500,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    },
    
    300: {
      dataType: data.geopotential300,
      process: (values) => {
        return values.map(item => process.processItem(item))
      },
    }
  },


  pressure: {
    surface: {
      dataType: data.pressure,
      process: (values) => {
        return values.map(item => process.processPressureItem(item))
      }
    }
  },

  dewpoint: {
    surface: {
      dataType: data.dewpoint,
      process: (values) => {
        return values.map(item => process.processTemperatureItem(item))
      },
    }
  },

  precipitation: {
    surface: {
      dataType: data.precipitation,
      process: (values) => {
        return values.map(item => process.processPrecipitationItem(item))
      }
    }
  },

}
