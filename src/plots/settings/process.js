const PRECIPITATION_FILTER_THRESHOLD = 0.1

const kelvinToCelsius = (value) => {
  return value - 272.15
}

const pascalToHectoPascal = (value) => {
  return value / 100
}


const processItem = (item) => {
  return {
    y: item.value,
    t: item.time
  }
}

const processTemperatureItem = (item) => {
  return {
    y: kelvinToCelsius(item.value),
    t: item.time
  }
}

const processPressureItem = (item) => {
  return {
    y: pascalToHectoPascal(item.value),
    t: item.time
  }
}

const processPrecipitationItem = (item) => {
  return {
    y: item.value > PRECIPITATION_FILTER_THRESHOLD ? item.value : 0,
    t: item.time
  }
}

export default {
  processItem,
  processTemperatureItem,
  processPressureItem,
  processPrecipitationItem,
}
