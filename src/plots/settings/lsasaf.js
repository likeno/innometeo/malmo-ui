import data from './data'
import process from './process'

export default {

  bui: {
    surface: {
      dataType: data.bui,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  dc: {
    surface: {
      dataType: data.dc,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  dmc: {
    surface: {
      dataType: data.dmc,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  ffmc: {
    surface: {
      dataType: data.ffmc,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  fwi: {
    surface: {
      dataType: data.fwi,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  isi: {
    surface: {
      dataType: data.isi,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  p2000: {
    surface: {
      dataType: data.p2000,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  p2000a: {
    surface: {
      dataType: data.p2000a,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },
  frm: {
    surface: {
      dataType: data.frm,
      process: (values) => {
        return values.map(item => process.processItem(item))
      }
    }
  },

}
