import axis from './axis'
import arome from './arome'
import ecmwf from './ecmwf'
import lsasaf from './lsasaf'

const data = {
  arome,
  ecmwf,
  lsasaf,
}

// Translates a string like 'arome.temperature.continent.surface' into
// the proper data setting that is registered with that path
const getSettingsForVariable = (name) => {
  return name.split('.').reduce((prev, curr) => prev && prev[curr], data)
}

// Transforms a object with nested objects into an array of path-like
// strings of the like 'arome.temperature.continent.surface'
const getDataPaths = (obj, prefix, store) => {
  for(let key in obj) {
    const curPath = `${prefix}.${key}`
    if(typeof obj[key] === 'object' && !('dataType' in obj[key])) {
      getDataPaths(obj[key], curPath, store)
    } else {
      store.push(curPath)
    }
  }

  return store
}

const getAxisById = (axisId) => {
  return Object.values(axis).find(item => {
    return item.id === axisId
  })
}

export default {
  axis,
  data,

  getSettingsForVariable,
  getDataPaths,
  getAxisById,
}
