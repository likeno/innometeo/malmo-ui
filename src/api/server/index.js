import axios from 'axios'

const BASE_API_URL = process.env.VUE_APP_BASE_API_URL

const services = BASE_API_URL + '/api/services/'
const layerGroups = BASE_API_URL + '/api/layergroups/'
const layers = BASE_API_URL + '/api/layers/'
const locationGroups = BASE_API_URL + '/api/locationgroups/'
const locations = BASE_API_URL + '/api/locations/'
const panes = BASE_API_URL + '/api/mappanes/'

const fetch = (url,) => {
  return axios.get(url)
}

export default {
  fetchServices() {
    return fetch(services).then((response) => { return response.data })
  },

  fetchLayerGroups() {
    return fetch(layerGroups).then((response) => { return response.data })
  },

  fetchLayers() {
    return fetch(layers).then((response) => { return response.data })
  },

  fetchLocationGroups() {
    return fetch(locationGroups).then((response) => { return response.data })
  },

  fetchLocations() {
    return fetch(locations).then((response) => { return response.data })
  },

  fetchPanes() {
    return fetch(panes).then((response) => { return response.data })
  },
}
