import services from './data/services'
import layerGroups from './data/layer-groups'
import layers from './data/layers'
import locationGroups from './data/location-groups'
import locations from './data/locations'

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockData)
    }, time)
  })
}

export default {
  fetchServices() {
    return fetch(services, 1000)
  },

  fetchLayerGroups() {
    return fetch(layerGroups, 1000)
  },

  fetchLayers() {
    return fetch(layers, 1000)
  },

  fetchLocationGroups() {
    return fetch(locationGroups, 1000)
  },

  fetchLocations() {
    return fetch(locations, 1000)
  },
}
