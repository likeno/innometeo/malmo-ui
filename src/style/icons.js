import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faLayerGroup,
  faMapMarker,
  faUsersCog,
  faCalendarAlt,
  faTimes,
  faArrowRight,
  faAngleLeft,
  faForward,
  faBackward,
  faDizzy,
  faClone,
  faWindowMinimize,
  faLink,
  faUnlink,
} from '@fortawesome/free-solid-svg-icons'

library.add(faLayerGroup)
library.add(faMapMarker)
library.add(faUsersCog)
library.add(faCalendarAlt)
library.add(faTimes)
library.add(faArrowRight)
library.add(faAngleLeft)
library.add(faForward)
library.add(faBackward)
library.add(faDizzy)
library.add(faClone)
library.add(faWindowMinimize)
library.add(faLink)
library.add(faUnlink)

import Vue from 'vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.component('font-awesome-icon', FontAwesomeIcon)
