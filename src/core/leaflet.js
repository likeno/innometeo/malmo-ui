import { Icon, Layer, Util, imageOverlay, CRS } from 'leaflet'

// Reset Leaflet Icon
delete Icon.Default.prototype._getIconUrl
Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})

export var WmsOverlay = Layer.extend({
  // Default options
  options: {
    crs: null,
    uppercase: false,
    attribution: '',
    opacity: 1,
    isBack: false,
    minZoom: 0,
    maxZoom: 18,
  },

  initialize: function(url, options) {
    this._url = url

    let params = {}, opts = {}
    for(var option in options) {
      if(option in this.options) {
        opts[option] = options[option]
      } else {
        params[option] = options[option]
      }
    }

    Util.setOptions(this, opts)
    this.wmsParams = Util.extend({}, this._getDefaultWmsParams(), params)
  },

  setParams: function(params) {
    Util.extend(this.wmsParams, params)
    this.update()
  },

  onAdd: function() {
    this.update()
  },

  onRemove: function(map) {
    if(this._currentOverlay) {
      map.removeLayer(this._currentOverlay)
      delete this._currentOverlay
    }

    if(this._currentUrl) {
      delete this._currentUrl
    }
  },

  getEvents: function() {
    return {
      moveend: this.update
    }
  },

  update: function() {
    if(!this._map) {
      return
    }

    this.updateWmsParams()
    var url = this.getImageUrl()
    if(this._currentUrl == url) {
      return
    }
    this._currentUrl = url

    let bounds = this._map.getBounds()
    let overlay = imageOverlay(url, bounds, { opacity: 0 })
    overlay.addTo(this._map)
    overlay.once('load', _swap, this)

    function _swap() {
      if(!this._map) {
        return
      }

      if(overlay._url != this._currentUrl) {
        this._map.removeLayer(overlay)
        return
      } else if(this._currentOverlay) {
        this._map.removeLayer(this._currentOverlay)
      }

      this._currentOverlay = overlay
      overlay.setOpacity(
        this.options.opacity ? this.options.opacity : 1
      )

      if(this.options.isBack) {
        overlay.bringToBack()
      } else {
        overlay.bringToFront()
      }
    }

    if(this._map.getZoom() < this.options.minZoom || this._map.getZoom() > this.options.maxZoom) {
      this._map.removeLayer(overlay)
    }
  },

  setOpacity: function(opacity) {
    this.options.opacity = opacity
    if(this._currentOverlay) {
      this._currentOverlay.setOpacity(opacity)
    }
  },

  bringToBack: function() {
    this.options.isBack = true
    if(this._currentOverlay) {
      this._currentOverlay.bringToBack()
    }
  },

  bringToFront: function() {
    this.options.isBack = false
    if(this._currentOverlay) {
      this._currentOverlay.bringToFront()
    }
  },

  updateWmsParams: function(map) {
    if(!map) {
      map = this._map
    }

    let bounds = map.getBounds()
    let size = map.getSize()
    let wmsVersion = parseFloat(this.wmsParams.version)
    let crs = this.options.crs | map.options.crs
    let projectionKey = wmsVersion >= 1.3 ? 'crs' : 'srs'
    let nw = crs.project(bounds.getNorthWest())
    let se = crs.project(bounds.getSouthEast())

    var params = {
      width: size.x,
      height: size.y,
    }

    params[projectionKey] = crs.code
    params.bbox = (
      wmsVersion >= 1.3 && crs === CRS.EPSG4326
        ? [se.y, nw.x, nw.y, se.x]
        : [nw.x, se.y, se.x, nw.y]
    ).join(',')

    Util.extend(this.wmsParams, params)
  },

  getImageUrl: function() {
    var uppercase = this.options.uppercase || false
    var pstr = Util.getParamString(this.wmsParams, this._url, uppercase)
    return this._url + pstr
  },
})

export function wmsOverlay(url, options) {
  return new WmsOverlay(url, options)
}
