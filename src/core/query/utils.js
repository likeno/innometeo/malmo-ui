import moment from 'moment'

export function generateTimeList(start, steps) {
  let timeList = []

  for(var i = 0; i < steps; i++) {
    timeList.push(moment.unix(start).clone().add(i, 'hours').format('YYYY-MM-DDTHH:00:00'))
  }

  return timeList
}
