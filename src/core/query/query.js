import axios from 'axios'
import { Evented, Util } from 'leaflet'

// URL used for all feature info queries
const QUERY_URL = process.env.VUE_APP_BASE_FEATUREINFO_URL


export var QueryBuilder = Evented.extend({
  initialize: function(name) {
    this._params = {
      layer: name
    }
  },

  setLatitude: function(value) {
    this._params['latitude'] = value
    return this
  },

  setLongitude: function(value) {
    this._params['longitude'] = value
    return this
  },

  setReferenceTime: function(value) {
    this._params['reference_time'] = value
    return this
  },

  setTime: function(value) {
    this._params['time'] = value
    return this
  },

  execute: function() {
    let url = QUERY_URL + Util.getParamString(this._params, QUERY_URL);
    return axios.get(url)
  }
})

export function queryBuilder(name) {
  return new QueryBuilder(name)
}
