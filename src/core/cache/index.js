import axios from 'axios'


export class Cache {
  constructor() {
    this._resources = {}
  }

  size() {
    return Object.keys(this._resources).length
  }

  empty() {
    return this.size() === 0
  }

  clear() {
    this._resources = {}
  }

  has(url) {
    return url in this._resources
  }

  load(url) {
    if(this.has(url)) {
      return new Promise((resolve, reject) => {
        resolve(this._resources[url])
      })
    }

    return axios.get(url).then((response) => {
      this._resources[url] = response.data
      return this._resources[url]
    })
    .catch((error) => {
      window.console.log('Failed to retrieve from', url)
      return null
    })
  }

  temp() {
    if(this.has(url)) {
      return new Promise((resolve, reject) => {
        resolve(this._resources[url])
      })
    }

    return axios.get(url).then((response) => {
      return response.data
    })
    .catch((error) => {
      window.console.log('Failed to retrieve from', url)
      return null
    })
  }
}


export const mainCache = new Cache()
