import { Evented } from 'leaflet'
import { select, mouse } from 'd3-selection'
import { axisBottom, axisLeft, axisRight } from 'd3-axis'
import { scaleLinear, scaleLog } from 'd3-scale'
import { format } from 'd3-format'
import { range, bisector } from 'd3-array'
import { line, curveBasis } from 'd3-shape'
import { speedUnits } from '@/core/units'

//import { skewt_data } from './mock/data'


const topp = 100
const basep = 1050
const plines = [1000, 850, 700, 500, 300, 200, 100]
const pticks = [950, 900, 800, 750, 650, 600, 550, 450, 400, 350, 250, 15]
//const blevels = [137, 114, 105, 96, 83]
const tan = Math.tan(55 * (Math.PI / 180))
const minTmp = -45
const maxTmp = 50
const barbSize = 17

const bisectTemp = bisector(function(d) { return d.press }).left


export var SkewT = Evented.extend({
  initialize: function(parentContainer, plotData) {
    this.parentContainer = parentContainer
    this.plotData = [plotData[0].sort((a, b) => {
      if(a.press > b.press) return -1
      if(a.press < b.press) return 1
      return 0
    })]

    const plotDataLength = this.plotData[0].length
    const barbsIntervals = [...Array(8).keys()].map(i => Math.log10(i + 1)).map(i => Math.round(i * plotDataLength))
    window.console.log(barbsIntervals)
    //const barbsIntervals = [0, 5, 10, 17, 27, 40, 53, 65]
    this.barbsData = barbsIntervals.map(i => this.plotData[0][i]).map(item => {
      return {
        ...item,
        wspdround: (speedUnits.kt(item.wspd) - speedUnits.kt(item.wspd) % 5)
      }
    })

    window.console.log(this.barbsData)

    this.container = select(this.parentContainer)
    this.size = this._getContainerSize()

    this._setupPlot()
    this.plot()
  },

  _getContainerSize: function() {
    return {
      width: this.parentContainer.getBoundingClientRect().height * 1,
      height: this.parentContainer.getBoundingClientRect().height,
    }
  },

  _setupPlot: function() {
    this._setupCanvas()
    this._setupAxes()
  },

  _setupCanvas: function() {
    this.svg = this.container.append('svg')
      .attr('width', this.size.width)
      .attr('height', this.size.height)

    this.background = this.svg.append('g')
      .attr('class', 'skewt-bakground')

    this.foreground = this.svg.append('g')
      .attr('class', 'skewt-foreground')

    this._buildBarbsDefinitions()

    this.barbs = this.svg.append('g')
      .attr('class', 'skewt-windbarbs')
  },

  _setupAxes: function() {
    this.x = scaleLinear().range([30, this.size.width - 10]).domain([minTmp, maxTmp])
    this.xAxis = axisBottom().scale(this.x).tickSize(0, 0).ticks(10)

    this.y = scaleLog().range([10, this.size.height - 20]).domain([topp, basep])
    this.yAxis = axisLeft().scale(this.y)
      .tickSize(0, 0)
      .tickValues(plines)
      .tickFormat(format('.0f'))
    this.yAxis2 = axisRight().scale(this.y)
      .tickSize(5, 0)
      .tickValues(pticks)
      .tickFormat(format('.0f'))
  },

  plot: function() {
    this._plotAxes()
    this._plotGrid()
    this._plotData()
    this._plotBarbs()
  },

  _plotAxes: function() {
    this.background.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + this.y(basep) + ')')
      .call(this.xAxis)

    this.background.append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + this.x(minTmp) + ', 0)')
      .call(this.yAxis)

    this.background.append('g')
      .attr('class', 'y axis ticks')
      .attr('transform', 'translate(' + this.x(minTmp) + ', 0)')
      .call(this.yAxis2)
  },

  _plotGrid: function() {
    var self = this

    this.clipper = this.background.append('clipPath')
      .attr('id', 'clipper').append('rect')
        .attr('x', self.x(minTmp))
        .attr('y', self.y(topp))
        .attr('width', this.size.width)
        .attr('height', this.size.height)


    this.background.selectAll('temperatureline')
      .data(range(-100, 50, 10)).enter().append('line')
        .attr('x2', function(d) { return self.x(d) - 0.5 + (self.y(basep) - self.y(100)) / tan })
        .attr('x1', function(d) { return self.x(d) - 0.5 })
        .attr('y1', function() { return self.y(basep) })
        .attr('y2', function() { return self.y(topp) })
        .attr('class', function(d) { return d === 0 ? 'tempzero' : 'gridline' })
        .attr('clip-path', 'url(#clipper)')

    this.background.selectAll('pressureline')
      .data(plines).enter().append('line')
        .attr('x1', function() { return self.x(minTmp) })
        .attr('x2', function() { return self.x(maxTmp) })
        .attr('y1', function(d) { return self.y(d) })
        .attr('y2', function(d) { return self.y(d) })
        .attr('class', 'gridline')

    var pp = range(topp, basep+1, 10)
    var dryad = range(-30, 240, 20)
    var all = []
    for(var i = 0; i < dryad.length; i++) {
      var z = []
      for(var j = 0; j < pp.length; j++) {
        z.push(dryad[i])
      }
      all.push(z)
    }

    var dryline = line().curve(curveBasis)
      .x(function(d, i) {
        return self.x((273.15 + d) / Math.pow((1000 / pp[i]), 0.286) - 273.15) + (self.y(basep) - self.y(pp[i])) / tan
      })
      .y(function(d, i) { return self.y(pp[i]) })

    this.background.selectAll('dryadiabaticline')
      .data(all).enter().append('path')
        .attr('class', 'gridline')
        .attr('clip-path', 'url(#clipper)')
        .attr('d', dryline)
  },

  _plotBarbs: function() {
    window.console.log('plot barbs')
    var self = this
    this.barbs.selectAll('barbs').data(this.barbsData).enter().append('use')
      .attr('xlink:href', function(d) { return '#barb' + d.wspdround })
      .attr('transform', function(d) {
        return 'translate(' + self.x(maxTmp - 2) + ',' + self.y(d.press) + ') rotate(' + (d.wdir + 90) + ')';
      })
  },

  _plotData: function() {
    var self = this
    var skewtline = self.plotData[0].filter(function(d) { return (d.temp > -1000 && d.dwpt > -1000) })
    var skewtlines = []
    skewtlines.push(skewtline)

    var temperatureline = line()//.curve(curveBasis)
      .x(function(d) { return self.x(d.temp) + (self.y(basep) - self.y(d.press)) / tan })
      .y(function(d) { return self.y(d.press) })

    this.foreground.selectAll('templines')
      .data(skewtlines).enter().append('path')
        .attr('class', function(d, i) { return (i < 10) ? 'temp skline' : 'temp mean' })
        .attr('clip-path', 'url(#clipper)')
        .attr('d', temperatureline)

    var dewpointline = line()//.curve(curveBasis)
      .x(function(d) { return self.x(d.dwpt) + (self.y(basep) - self.y(d.press)) / tan })
      .y(function(d) { return self.y(d.press) })

    this.foreground.selectAll('dewlines')
      .data(skewtlines).enter().append('path')
        .attr('class', function(d, i) { return (i < 10) ? 'dwpt skline' : 'dwpt mean' })
        .attr('clip-path', 'url(#clipper)')
        .attr('d', dewpointline)

    this._loadTooltips(skewtlines[0])
  },

  _loadTooltips: function(data) {
    var self = this
    var lines = data.reverse()

    var textcontainer = this.foreground.append('g')

    var tmpcfocus = textcontainer.append('g')
      .attr('class', 'focus tmpc')
      .style('display', 'none')

    var dwptfocus = textcontainer.append('g')
      .attr('class', 'focus dwptc')
      .style('display', 'none')

    dwptfocus.append('circle').attr('r', 4)
    dwptfocus.append('text').attr('x', -9).attr('dy', '.35em')

    tmpcfocus.append('circle').attr('r', 4)
    tmpcfocus.append('text').attr('x', 9).attr('dy', '.35em')

    this.svg.append('rect')
      .attr('class', 'overlay')
      .attr('width', this.size.width)
      .attr('height', this.size.height)
      .on('mouseover', () => {
        tmpcfocus.style('display', null)
        dwptfocus.style('display', null)
      })
      .on('mouseout', () => {
        tmpcfocus.style('display', 'none')
        dwptfocus.style('display', 'none')
        self.fire('level-data-changed', {})
      })
      .on('mousemove', function() {
        var y0 = self.y.invert(mouse(this)[1])
        var i = bisectTemp(lines, y0, 1, lines.length - 1)
        var d0 = lines[i - 1]
        var d1 = lines[i]
        var d = y0 - d0.press > d1.press - y0 ? d1 : d0;

        self.fire('level-data-changed', d)

        let xTmpTranslate = (self.x(d.temp) + (self.y(basep) - self.y(d.press)) / tan)
        let yTmpTranslate = self.y(d.press)
        tmpcfocus.attr('transform', `translate(${xTmpTranslate}, ${yTmpTranslate})`)
        tmpcfocus.select('text').text(Math.round(d.temp) + 'ºC')

        let xDwptTranslate = (self.x(d.dwpt) + (self.y(basep) - self.y(d.press)) / tan)
        let yDwptTranslate = self.y(d.press)
        dwptfocus.attr('transform', `translate(${xDwptTranslate}, ${yDwptTranslate})`)
        dwptfocus.select('text').text(Math.round(d.dwpt) + 'ºC')

      })
  },

  _buildBarbsDefinitions: function() {
    const speeds = range(5, 105, 5)
    this.barbdefs = this.svg.append('defs')

    speeds.forEach((speed) => {
      const currentBarb = this.barbdefs.append('g').attr('id', 'barb' + speed)
      let flags = Math.floor(speed / 50)
      let pennants = Math.floor((speed - flags * 50) / 10)
      let halfPennants = Math.floor((speed - flags * 50 - pennants * 10) / 5)
      let px = barbSize/2

      currentBarb.append('line').attr('x1', 0).attr('x2', 0).attr('y1', -barbSize/2).attr('y2', barbSize/2)

      // Draw flags for each steam
      for(let i = 0; i < flags; i++) {
        currentBarb.append('polyline')
          .attr('points', '0,' + px + ' -10,' + (px) + ' 0,' + (px - 4))
          .attr('class', 'flag')
        px -= 7
      }

      // Draw pennants
      for(let i = 0; i < pennants; i++) {
        currentBarb.append('line')
          .attr('x1', 0).attr('x2', -8)
          .attr('y1', px).attr('y2', px + 4)
        px -= 3
      }

      // Draw half pennants
      for(let i = 0; i < halfPennants; i++) {
        currentBarb.append('line')
          .attr('x1', 0).attr('x2', -5)
          .attr('y1', px).attr('y2', px + 2)
        px -= 3
      }
    })
  },
})

export function skewT(container, plotData) {
  return new SkewT(container, plotData)
}
