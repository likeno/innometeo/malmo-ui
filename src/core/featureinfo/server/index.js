import axios from 'axios'
import moment from 'moment'
import { Util } from 'leaflet'

const FEATURE_INFO_URL = 'http://127.0.0.1:8005/query'

export function getFeatureInfo(params) {
  let queryString = Util.getParamString(params)
  let requestUrl = FEATURE_INFO_URL + queryString
  return axios.get(requestUrl)
}

export function generateTimeList(startTime, numSteps) {
  let timeList = []

  for(var i = 0; i < numSteps; i++) {
    timeList.push(moment.unix(startTime).add(i, 'hours').format('YYYY-MM-DDTHH:00:00[Z]'))
  }

  return timeList
}
