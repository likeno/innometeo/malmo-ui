import sampleData from './data/sample'

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockData)
    }, time)
  })
}

export function getFeatureInfo() {
  return fetch(sampleData, 2000)
}

export function generateTimeList() {
  let timeList = []
  return timeList
}
