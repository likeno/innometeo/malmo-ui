// Utilities and methods used to process getfeatureinfo data
import moment from 'moment'

export function extractLayerByName(data, name) {
  return data.properties.hasOwnProperty(name) ? data.properties[name] : null
}

export function extractDatetimes(data) {
  return data.map((item) => {
    return moment(item.time).unix()
  }).sort()
}

export function extractDates(data) {
  let datetimes = extractDatetimes(data)
  let dates = new Set()

  datetimes.forEach((item) => {
    return dates.add(moment.unix(item).format('YYYY-MM-DD'))
  })

  return Array.from(dates).map((item) => {
    return moment(item).unix()
  }).sort()
}
