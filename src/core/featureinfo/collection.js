import { Evented, Util } from 'leaflet'


const FEATURE_INFO_URL = 'http://192.168.156.27:8000/query'

export var FeatureInfoDataCollection = Evented.extend({
  initialize: function() {
    this._layers = []
  },

  addLayer: function(name) {
    this._layers.push(name)
  },

  retrieve: function(latitude, longitude, referenceTime, time) {
    let baseParams = {
      latitude: latitude,
      longitude: longitude,
      reference_time: referenceTime,
      time: time,
    }

    return this._layers.map((item) => {
      let param = Util.extend({ layer: item }, baseParams)
      let url = FEATURE_INFO_URL + Util.getParamString(params, FEATURE_INFO_URL)
      return axios.get(url)
    })
  },
})

export function featureInfoDataCollection() {
  return new FeatureInfoDataCollection()
}
