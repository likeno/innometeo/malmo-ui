import { Util } from 'leaflet'

export class ForecastItem {
  constructor(name, value) {
    this._name = name
    this._value = value
  }

  get name() {
    return this._name
  }

  get value() {
    return this._value
  }

  getValueWithFormat(format) {
    return Util.formatNum(this.value, format)
  }
}


export class FeatureInfoDataProxy {
  constructor(referenceTime, data) {
    this._referenceTime = referenceTime
    this._data = data
    this._translators = {}
  }

  registerTranslator(name, trueName) {
    this._translators[name] = trueName
  }

  getValueForDatetime(name, datetime) {
    let trueName = this._translateVariable(name)
    
    return this._data[trueName].find((item) => {
      return moment.unix(datetime).isSame(moment(item.time))
    })
  }

  _translateVariable(name) {
    return this._translators[name]
  }
}


export const AromeContinentFeatureInfoProxy = {}
export const AromeMadeiraFeatureInfoProxy = {}
export const AromeAzoresFeatureInfoProxy = {}
export const EcmwfFeatureInfoProxy = {}
