import moment from 'moment'
import axios from 'axios'
import { Util } from 'leaflet'


function createQueryUrl(url, params) {
  return url + Util.getParamString(params, url)
}


export function generateSteps(start) {
  let startDate = moment.unix(start)
  let steps = []

  for(let i = 0; i < 48; i++) {
    steps.push(startDate.clone().add(i, 'hour').format('YYYY-MM-DDTHH:00:00'))
  }

  return steps
}


export class FeatureInfoData {
  constructor(name) {
    this._name = name
  }

  get name() {
    return this._name
  }

  get(latitude, longitude, referenceTime, timeGeneratorCallback) {
    let times = timeGeneratorCallback(referenceTime)
    let params = {
      latitude: latitude,
      longitude: longitude,
      reference_time: moment.unix(referenceTime).format('YYYY-MM-DDTHH:00:00[Z]'),
      time: times,
      layer: this.name,
    }
    let url = createQueryUrl('http://192.168.156.27:8000/query', params)
    return axios.get(url)
  }
}
