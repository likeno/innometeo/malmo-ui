export const buildQueryString = (params) => {
  const queryString = new URLSearchParams()
  Object.entries(params).forEach(([key, value]) => {
    if(Array.isArray(value))
      value.forEach(item => queryString.append(key, item))
    else
      queryString.append(key, value)
  })
  return queryString.toString()
}


export const buildUrlWithQuery = (baseUrl, queryString) => {
  return `${baseUrl}?${queryString}`
}
