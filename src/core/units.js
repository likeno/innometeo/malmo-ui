import { Util } from 'leaflet'

// Functions used to convert between different units
export const temperatureUnits = {
  _baseunit: 'K',
  K: (value) => { return value },
  C: (value) => { return value - 272.15 },
}

export const pressureUnits = {
  _baseunit: 'Pa',
  Pa: (value) => { return value },
  hPa: (value) => { return value / 100 },
}

export const speedUnits = {
  _baseunit: 'ms',
  ms: (value) => { return value },
  kmh: (value) => { return value * 3.6 },
  kt: (value) => { return value * 1.9438 },
}

export const formatNum = (value, decimals) => {
  return Util.formatNum(value, decimals)
}

export default {
  temperatureUnits,
  pressureUnits,
  speedUnits,
  formatNum,
}
