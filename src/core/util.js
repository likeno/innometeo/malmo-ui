let _id_counter = 0;

export function createUniqueId() {
  return 'id' + _id_counter++;
}


export function findParentComponent(vueParentElement, criteria) {
  let found = false

  while(vueParentElement && !found) {
    if(!criteria(vueParentElement)) {
      vueParentElement = vueParentElement.$parent
    } else {
      found = true
    }
  }

  return vueParentElement
}


export function findParentComponentByName(vueParentElement, name) {
  return findParentComponent(vueParentElement, (el) => {
    return el.$options.name === name
  })
}
