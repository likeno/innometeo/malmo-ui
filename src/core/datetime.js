import moment from 'moment'

const now = moment.utc()

const MAXIMUM_TIME_STEP = 72
const MINIMUM_TIME_STEP = 12

export function getCurrentDate() {
  return moment(moment.utc().format('YYYY-MM-DD')).unix()
}

export function getCurrentHour() {
  return moment.utc().hour()
}

/**
 * Compute the reference time from the current time. Returns the
 * computed reference time in unix timestamp format.
 */
export function getReferenceTime() {
  return now.hour() >= 21
    ? now.clone().startOf('day').add(12, 'h').unix()
    : now.clone().startOf('day').unix()
}

/**
 * Returns the current time rounded to the start of the current hour in
 * unix timestamp format.
 */
export function getCurrentTime() {
  return now.clone().startOf('hour').unix()
}

/**
 * Returns the maximum timestamp for the time slider in unix timestamp
 * format.
 */
export function getMaxTime() {
  const referenceTime = moment.unix(getReferenceTime())
  return referenceTime.clone().add(MAXIMUM_TIME_STEP, 'h').unix()
}

/**
 * Returns the minimum timestamp for the time slider in unix timestamp
 * format.
 */
export function getMinTime() {
  const currentTime = moment.unix(getCurrentTime())
  return currentTime.clone().subtract(MINIMUM_TIME_STEP, 'h').unix()
}

/**
 * Returns a list os unix timestamps used for the time slider.
 */
export function getTimeSteps() {
  // 12 hours * 4 15min intervals
  const observationSteps = [...Array(12 * 12).keys()].map((i) => {
    return moment.unix(getMinTime()).clone().add(i * 5, 'minutes')
  })

  const forecastStepCount = (getMaxTime() - getCurrentTime()) / 3600
  const forecastSteps = [...Array(forecastStepCount).keys()].map(i => {
    return moment.unix(getCurrentTime()).clone().add(i, 'hours')
  })

  return [...observationSteps, ...forecastSteps].map(i => i.clone().unix())
}
