import uuid from 'uuid'

export const stationsServiceId = uuid()
export const stationsLayerGroupId = uuid()

export function createStationsService (store) {
  store.commit('ADD_SERVICES', {
    services: [{
      id: stationsServiceId,
      name: 'Estações',
      url: process.env.VUE_APP_BASE_STATIONS_URL
    }]
  })
}

export function createStationsLayerGroup (store) {
  store.commit('ADD_LAYER_GROUPS', {
    layerGroups: [{
      id: stationsLayerGroupId,
      title: 'Estações'
    }]
  })
}

export function createStationsLayers (store) {
  store.commit('ADD_LAYERS', {
    layers: [{
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'pressure',
      service: stationsServiceId,
      status: false,
      title: 'Pressão',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'temperature-minimum',
      service: stationsServiceId,
      status: false,
      title: 'Temperatura mínima',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'temperature-mean',
      service: stationsServiceId,
      status: false,
      title: 'Temperatura média',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'temperature-maximum',
      service: stationsServiceId,
      status: false,
      title: 'Temperatura máxima',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'relative-humidity-minimum',
      service: stationsServiceId,
      status: false,
      title: 'Humidade relativa mínima',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'relative-humidity-mean',
      service: stationsServiceId,
      status: false,
      title: 'Humidade relativa média',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'relative-humidity-maximum',
      service: stationsServiceId,
      status: false,
      title: 'Humidade relativa máxima',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'wind-direction-mean',
      service: stationsServiceId,
      status: false,
      title: 'Direção média do vento',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'wind-direction-maximum',
      service: stationsServiceId,
      status: false,
      title: 'Direção do vento máximo',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'wind-intensity-mean',
      service: stationsServiceId,
      status: false,
      title: 'Intensidade média do vento',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'wind-maximum-intensity',
      service: stationsServiceId,
      status: false,
      title: 'Intensidade máxima do vento',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'precipitation',
      service: stationsServiceId,
      status: false,
      title: 'Precipitação',
      type: 1,
    }, {
      groups: [stationsLayerGroupId],
      icon: 'wi-fire',
      id: uuid(),
      name: 'precipitation-maximum',
      service: stationsServiceId,
      status: false,
      title: 'Precipitação máxima',
      type: 1,
    }]
  })
}
