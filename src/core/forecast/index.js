import { latLngBounds, latLng } from 'leaflet'

export const ECMWF = {
  bounds: latLngBounds(latLng(46.75, -37.05), latLng(27.9624, 0.0802303)),
  name: 'ecmwf',
  title: 'ECMWF',
}

export const AROME_CONTINENT = {
  bounds: latLngBounds(latLng(44.8, -12.8), latLng(33.9936, -1.2015)),
  name: 'arome_continent',
  title: 'AROME - Continent',
  data: {
    temperature: 'arome.2m.temperature.continent',
    precipitation: 'arome.0m.precipitation.continent',
    humidity: 'arome.2m.relative_humidity.continent',
  },
}

export const AROME_MADEIRA = {
  bounds: latLngBounds(latLng(34.75, -18.99), latLng(30.9823, -14.786)),
  name: 'arome_madeira',
  title: 'AROME - Madeira',
  data: {
    temperature: 'arome.2m.temperature.madeira',
    precipitation: 'arome.0m.precipitation.madeira',
    humidity: 'arome.2m.relative_humidity.madeira',
  },
}

export const AROME_AZORES = {
  bounds: latLngBounds(latLng(40.86, -33.09), latLng(35.7221, -24.0688)),
  name: 'arome_azores',
  title: 'AROME - Açores',
  data: {
    temperature: 'arome.2m.temperature.azores',
    precipitation: 'arome.0m.precipitation.azores',
    humidity: 'arome.2m.relative_humidity.azores',
  },
}

// Checks if a given point is inside the forecast area for a given forecast
// model.
export function isPointInsideModelArea(point, model) {
  return model.bounds.contains(point)
}

// Returns an array with all forecast models for which the given point is
// inside the forecast area.
export function getModelsForPoint(point) {
  let models = [ ECMWF, AROME_CONTINENT, AROME_MADEIRA, AROME_AZORES ]
  return models.filter((model) => { return isPointInsideModelArea(point, model) })
}

// Tries to return the forecast model from a layer name
export function getModelFromLayerName(layer) {
  let split = layer.split('.')

  if(split.length === 4 && split[0] === 'arome') {
    if(split[3] === 'continent') return AROME_CONTINENT
    if(split[3] === 'madeira') return AROME_MADEIRA
    if(split[3] === 'azores') return AROME_AZORES
  } else if(split.length === 3 && split[0] === 'ecmwf') {
    return ECMWF
  }

  return undefined
}
