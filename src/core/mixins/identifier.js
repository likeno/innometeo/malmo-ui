export default {
  props: {
    identifier: {
      type: String,
      required: true
    }
  },
}
