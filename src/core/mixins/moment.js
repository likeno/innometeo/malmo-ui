import moment from 'moment'

const ISO_FORMAT = 'YYYY-MM-DDTHH:mm:ss[Z]'

export default {
  filters: {
    unixToISO(value) {
      return moment.unix(value).format(ISO_FORMAT)
    },

    unixToFormat(value, formatString) {
      return moment.unix(value).format(formatString)
    },
  },
}
