import { Util } from 'leaflet'

export default {
  filters: {
    // Temperature filters
    kelvinToCelsius(value) {
      return parseFloat(value) - 273.5
    },

    celsiusToKelvin(value) {
      return parseFloat(value) + 273.5
    },

    // Velocity filters
    msToKmh(value) {
      return parseFloat(value) * 3.6
    },

    kmhToMs(value) {
      return parseFloat(value) / 3.6
    },

    // Pressure filters
    pascalToHectoPascal(value) {
      return parseFloat(value) / 100
    },

    hectoPascalToPascal(value) {
      return parseFloat(value) * 100
    }

    // Format numeric values
    formatNum(value, decimals) {
      return Util.formatNum(value, decimals)
    }
  }
}
