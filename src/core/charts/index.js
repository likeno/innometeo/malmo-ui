const axisMappings = {
  temperature: 'temperatureAxis',
  pressure: 'pressureAxis',
  humidity: 'humidityAxis'
}


const dataTypes = {
  'arome.temperature.continent': 'temperature',
  'arome.dewpoint.continent': 'temperature',
  'arome.relative_humidity.continent': 'humidity'
}



