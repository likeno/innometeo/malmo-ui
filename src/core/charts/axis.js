export const temperatureAxis = {
  id: 'temperatureAxis',
  type: 'linear',
}

export const pressureAxis = {
  id: 'pressureAxis',
  type: 'linear',
  ticks: {
    beginAtZero: true
  }
}

export const humidityAxis = {
  id: 'humidityAxis',
  type: 'linear',
  ticks: {
    beginAtZero: true,
  }
}

export setAxisPosition(axis, position) {
  return Object.assign({ position: position }, axis)
}

export setAxisLeft(axis) {
  return setAxisPosition(axis, 'left')
}

export setAxisRight(axis) {
  return setAxisPosition(axis, 'right')
}
